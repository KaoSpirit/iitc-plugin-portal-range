// ==UserScript==
// @id             iitc-plugin-portal-range@kaospirit
// @name           IITC plugin: Portal Range
// @category       Layer
// @version        0.0.2.2017-04-06
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/KaoSpirit/iitc-plugin-portal-range/raw/master/iitc-plugin-portal-range.meta.js
// @downloadURL    https://gitlab.com/KaoSpirit/iitc-plugin-portal-range/raw/master/iitc-plugin-portal-range.user.js
// @description    Shows the range of the portals (40 meters).
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==