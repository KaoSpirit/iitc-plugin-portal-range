# IITC Plugin: Portal Range
#### Based on zaprange plugin
##### Plugin for Ingress Intel Total Conversion, that shows 40 meters range around all portals. Helpful for creation traces for missions etc.
##### [Download link](https://gitlab.com/KaoSpirit/iitc-plugin-portal-range/raw/master/iitc-plugin-portal-range.user.js) 
