// ==UserScript==
// @id             iitc-plugin-portal-range@kaospirit
// @name           IITC plugin: Portal Range
// @category       Layer
// @version        0.0.2.2017-04-06
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/KaoSpirit/iitc-plugin-portal-range/raw/master/iitc-plugin-portal-range.meta.js
// @downloadURL    https://gitlab.com/KaoSpirit/iitc-plugin-portal-range/raw/master/iitc-plugin-portal-range.user.js
// @description    Shows ranges of portals (40 meters).
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==




function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== "function") window.plugin = function() {};



// PLUGIN START ///////////////////////////////////////////////////////

  // use own namespace for plugin
  window.plugin.range = function() {};
  window.plugin.range.rangeLayers = {};
  window.plugin.range.MIN_MAP_ZOOM = 15;

  window.plugin.range.portalAdded = function(data) {
    data.portal.on("add", function() {
      window.plugin.range.draw(this.options.guid, this.options.team);
    });

    data.portal.on("remove", function() {
      window.plugin.range.remove(this.options.guid, this.options.team);
    });
  };

  window.plugin.range.remove = function(guid, faction) {
    var previousLayer = window.plugin.range.rangeLayers[guid];
    if(previousLayer) {
      window.plugin.range.rangeCircleHolderGroup.removeLayer(previousLayer);
      delete window.plugin.range.rangeLayers[guid];
    }
  };

  window.plugin.range.draw = function(guid, faction) {
    var d = window.portals[guid];

    var coo = d._latlng;
    var latlng = new L.LatLng(coo.lat,coo.lng);
    var portalLevel = d.options.level;
    var optCircle = {color:"purple",opacity:0.7,fillColor:"purple",fillOpacity:0.1,weight:1,clickable:false, dashArray: [10,6]};
    var range = 40;

    var circle = new L.Circle(latlng, range, optCircle);

    circle.addTo(window.plugin.range.rangeCircleHolderGroup);
    window.plugin.range.rangeLayers[guid] = circle;
  };

  window.plugin.range.showOrHide = function() {
    if(map.getZoom() >= window.plugin.range.MIN_MAP_ZOOM) {
      // show the layer
      if(!window.plugin.range.rangeLayerHolderGroup.hasLayer(window.plugin.range.rangeCircleHolderGroup)) {
        window.plugin.range.rangeLayerHolderGroup.addLayer(window.plugin.range.rangeCircleHolderGroup);
        $(".leaflet-control-layers-list span:contains('Portal range')").parent("label").removeClass("disabled").attr("title", "");
      }
    } else {
      // hide the layer
      if(window.plugin.range.rangeLayerHolderGroup.hasLayer(window.plugin.range.rangeCircleHolderGroup)) {
        window.plugin.range.rangeLayerHolderGroup.removeLayer(window.plugin.range.rangeCircleHolderGroup);
        $(".leaflet-control-layers-list span:contains('Portal range')").parent("label").addClass("disabled").attr("title", "Zoom in to show those.");
      }
    }
  };

  var setup =  function() {
    // this layer is added to the layer chooser, to be toggled on/off
    window.plugin.range.rangeLayerHolderGroup = new L.LayerGroup();

    // this layer is added into the above layer, and removed from it when we zoom out too far
    window.plugin.range.rangeCircleHolderGroup = new L.LayerGroup();

    window.plugin.range.rangeLayerHolderGroup.addLayer(window.plugin.range.rangeCircleHolderGroup);

    // to avoid any favouritism, we"ll put the player"s own faction layer first
    window.addLayerGroup("Portal range", window.plugin.range.rangeLayerHolderGroup);

    window.addHook("portalAdded", window.plugin.range.portalAdded);

    map.on("zoomend", window.plugin.range.showOrHide);

    window.plugin.range.showOrHide();
  };

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the "setup" function
if(window.iitcLoaded && typeof setup === "function") setup();
} // wrapper end
// inject code into site context
var script = document.createElement("script");
var info = {};
if (typeof GM_info !== "undefined" && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode("("+ wrapper +")("+JSON.stringify(info)+");"));
(document.body || document.head || document.documentElement).appendChild(script);
